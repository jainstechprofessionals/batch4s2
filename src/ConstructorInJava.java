
public class ConstructorInJava {

	int a;

	public ConstructorInJava() {
		System.out.println("in constructor");
	}

	public void ThisKeywordInJava() {

	}

	public ConstructorInJava(String s) {

		System.out.println("in paranet constructor");
	}

	public void init(int i) {
		a = i;
	}

	public int mridul() {
		return 0;
	}

	public void show() {

		System.out.println("in show  " + a);
	}

	public void display() {
		System.out.println("in display " + a);
	}

	public void compare(int i, int j, int k) {
		System.out.println(i);
		System.out.println(j);
		System.out.println(k);
	}

	public static void main(String[] args) {
		ConstructorInJava obj = new ConstructorInJava();
		obj.display();
		ConstructorInJava obj1 = new ConstructorInJava("rounka");

	}

}
