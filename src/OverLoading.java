
public class OverLoading {

	public OverLoading(){
		System.out.println("in non param");
	}
	public OverLoading(int i){
		System.out.println("in non param");
	}
	
	public void sum(int i, int j) {
		System.out.println(i + j);
	}

	public void sum(float i, int j) {
		System.out.println(i + j);
	}

	public void sum(int i, int j, int k) {
		System.out.println(i + j + k);
	}

	public static void main(String[] ronak) {

		OverLoading obj = new OverLoading();
		OverLoading obj1 = new OverLoading(12);
		obj.sum(12, 2);
		obj.sum(12, 3, 4);
	}

	

}
