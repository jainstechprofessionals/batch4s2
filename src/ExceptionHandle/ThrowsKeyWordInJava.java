package ExceptionHandle;

public class ThrowsKeyWordInJava {
	
	
	public void show() throws InterruptedException  {
		Thread.sleep(4000);
	}

	public static void main(String[] args) {
		ThrowsKeyWordInJava obj  = new ThrowsKeyWordInJava();
		try {
			obj.show();
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
	
		
	}

}
