package ExceptionHandle;

public class ThrowKeywordInJava {

	public void sum() {
		System.out.println(10/0);
	}
	
	public void divi() {
		sum();
	}
	
	
	
	
	public  void calculateAge(int age) throws ArithmeticException ,ArrayIndexOutOfBoundsException{
		
		if(age<18) {
			System.out.println("you are not elligible for this web Series");
			throw new ArithmeticException();
		}else {
			System.out.println("you  are elligible");
		}
		
		
	}
	
	public static void main(String[] args) {
		
		ThrowKeywordInJava obj = new ThrowKeywordInJava();
//		obj.calculateAge(13);
//		System.out.println("hello gurus");
		
		try {
			obj.divi();
		}catch(ArithmeticException e) {
			System.out.println("enter non zero");
		}
	}

}
