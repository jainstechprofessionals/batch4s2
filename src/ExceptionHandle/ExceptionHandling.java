package ExceptionHandle;

public class ExceptionHandling {

	public static void main(String[] args) {

		try {

			try {

			} catch (Exception e) {

			}

			int a = 10;
			int b = 10;
			String str = "null";
			int ap[] = new int[3];
//			ap[3] = 9;
			System.out.println(a / b);
			System.out.println(str.length());
			System.out.println("hello");

		} catch (ArithmeticException e) {
			e.printStackTrace();
			System.out.println("please enter a valid value of b");
		} catch (NullPointerException n) {
//			n.printStackTrace();
			System.out.println("string is null");
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("trying to add a value which is greater then the size of array");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("some exceptions is there");
			e.printStackTrace();
		}

		finally {
			System.out.println("in finally");
		}
		// try, catch , finally , throws ,throw

	}

}
