package AbstractionInJava;

public abstract  class AbstractionInJava {

	public void show() {
		System.out.println("in show");
	}
	public abstract void getComputer();
	
	public abstract void getComputer(int i);
}
