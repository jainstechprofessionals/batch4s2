package AbstractionInJava;

public class ObjectFactory {

	private  ObjectFactory() {
		
	}
	
	
	public static SBI returnSBIDeatails() {
		return new SBI();
	}
	
	public static PolicyBaazar returnBank(String bankName) {
		PolicyBaazar obj;
		if (bankName.equals("SBI")) {
			obj = new SBI(); // run time poly, Dynamic Method dispatch, Late binding, upcasting
		} else if (bankName.equals("HSBC")) {
			obj = new HSBC();
		} else {
			obj=null;
			System.out.println("please enter a valid bank name");			
		}
		return obj;
		
	}
	
	
}
