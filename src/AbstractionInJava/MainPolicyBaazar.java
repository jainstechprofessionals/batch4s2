package AbstractionInJava;

import java.util.Scanner;

public class MainPolicyBaazar {

	public static void main(String[] args) {

		PolicyBaazar obj;
//		Scanner sc = new Scanner(System.in);
//		System.out.println("please enter the bank name");
//		String str = sc.nextLine();
      String str = "SBI";

		if (str.equals("SBI")) {
			obj = new SBI(); // run time poly, Dynamic Method dispatch, Late binding, upcasting
		} else if (str.equals("HSBC")) {
			obj = new HSBC();
		} else {
			obj = null;
			System.out.println("please enter a valid bank name");
			System.exit(0);
		}

		
		obj.getRateOfInterest();
		obj.getFDInterest();

	}

}
