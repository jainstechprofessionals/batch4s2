package InheritanceInJava;

public class Dog extends Animal {

	Dog() {
		super("poonam");
		System.out.println("in Dog");
	}

	public void show() {

		System.out.println("In Dog Show");
	}

	public void bark() {

		System.out.println("Dog :in bark ");
	}
}
