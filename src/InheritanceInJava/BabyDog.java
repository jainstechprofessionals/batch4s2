package InheritanceInJava;

public class BabyDog extends Dog{

	public BabyDog() {
		super();
		System.out.println("in BabyDog");
	}
	
	public void cry() {
		System.out.println("babyDog :in cry ");
	}
}
