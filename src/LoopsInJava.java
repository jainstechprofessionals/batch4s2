
public class LoopsInJava {

	public static void main(String[] args) {
		
		
		// entry control  (while, for)
		// exit control (do while)
		
		int a=2;
		
		while(a<=20) {
			System.out.println(a);
			a=a+2;
		}
		

		
//		for(int a=2;a<=20;a=a+2) {
//			System.out.println(a);
//		}
		
//		
//		int a =2;
//		
//		do {
//			System.out.println(a);
//			a=a+2;
//		}while(a<=20);
//		
		
		
		
	}

}
