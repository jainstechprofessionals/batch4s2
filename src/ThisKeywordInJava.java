
public class ThisKeywordInJava {

	int a; // instance

	ThisKeywordInJava(){
		this(12,12);
		System.out.println("in non param");
	}
	ThisKeywordInJava(int a) {
	
		System.out.println("in param");
		
		this.a = a;
		
	}
	
	ThisKeywordInJava(int a,int b) {
		
		System.out.println("in 2 parameter constructor");
		this.a = a;
	}

	public void show(int a) {
				
		this.display();
		System.out.println(a); // 12

		System.out.println(this.a);
	}
	
	public void display() {
		 System.out.println("in display");
	}
	

	public static void main(String[] args) {

		ThisKeywordInJava obj = new ThisKeywordInJava(24);
		obj.show(12);
		
		ThisKeywordInJava obj1 = new ThisKeywordInJava(24,4);

	}

}
