
public class Backup1 {

	public static void main(String[] args) {

		int a=10;
		
		// Primitive
		// Non Primitive
		
		
		boolean b = true; //  1 bit
		char c ='A';  // 2 byte
		
		byte by = 12; // 1 byte   // -128 to 127
		short s= 34;  // 2 byte   // -32768 to 32767
		int i = 134; // 4 byte     // -2^31 to 2^31-1
		long l= 234; // 8 byte    // -2^63 to 2^63-1
		
		float f=12.23f;  // 4 byte
		double d = 12.3d; // 8 byte
		
		
		s=by;  // implicit conversion
		System.out.println(s);
		
		by=(byte) s; /// explicit conversion
		
		
		f= i;
		
		System.out.println(f);
		
		i=(int) f;
		
		System.out.println(i);
		
		
	}
}
