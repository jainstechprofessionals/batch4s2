
public class Variables {

	int a = 10; // instance variable
	int b=23;

	public void print() {
		System.out.println(a);
	}

	public void show() {
		System.out.println(a);
		int a = 45; // local variable
		System.out.println(a);
	}

	public void display(int a) {
		System.out.println(a);
	}

	public static void main(String[] args) {
		Variables obj = new Variables();
		Variables obj1 = new Variables();
		obj1.a=56;
		obj.show();
		System.out.println("***********");
		obj1.show();
//		obj.display(33);
//		obj.print();
	}

}
