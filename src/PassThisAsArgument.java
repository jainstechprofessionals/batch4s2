
public class PassThisAsArgument {

	 int a;
	
	public void show() {
		display(this);
	}
	
	public void s(int a) {
		
	}
	
	public PassThisAsArgument returnObject() {
		return this;
	}
	
	public void print() {
		PassThisAsArgument onh = new PassThisAsArgument();
		this.display(onh);
		s(10);
	}
	
	public void display(PassThisAsArgument o) {
		o.a=10;
	}
	
	public static void main(String[] args) {
		
		
		PassThisAsArgument obj = new PassThisAsArgument();
		
		PassThisAsArgument p =obj.returnObject();

	}

}
