
public class MethodProgram {

	
	
	
	public void show() {
		System.out.println("in show");
		int a = 20;
		int b = 40;
		System.out.println(b / a); // 2
		System.out.println(b % a); // 0
	}

	public boolean evenOrOdd(int i) {
		if (i % 2 == 0) {
			return true;
		} else {
			return false;
		}
	}

	public int sum(int i, int j) {
		int temp = i + j;
		return temp;
	}

	public void leapYear(int year) {
		if (year % 4 == 0) {
			System.out.println("leap year");
		} else {
			System.out.println("not a leap year");
		}
	}

	public static void main(String[] args) {
		MethodProgram obj = new MethodProgram();
		int n = 4;
		boolean b = obj.evenOrOdd(n);

		if (b == true) {
			obj.leapYear(n);
		} else {
			System.out.println("odd number cannot be leap year");
		}

		int s = obj.sum(23, 2);
		System.out.println(s);

	}

}
